import axios from 'axios';
import store from '../Service/Store';

export const baseUrl = 'https://60cb2f6921337e0017e440a0.mockapi.io';

const defaultHeader = () => {
  const token = store.getState().user.token;
  return {
    Authorization: token,
  };
};

export const request = {
  get: async (prefix, header = null) => {
    return axios
      .get(baseUrl + prefix, {
        headers: header !== null ? header : defaultHeader(),
      })
      .then(res => Promise.resolve({data: res.data, status: res.status}))
      .catch(err => Promise.reject(err));
  },
  post: async (prefix, body, header = null) => {
    return axios
      .post(baseUrl + prefix, body, {
        headers: header !== null ? header : defaultHeader(),
      })
      .then(res => Promise.resolve({data: res.data, status: res.status}))
      .catch(err => Promise.reject(err));
  },
  delete: async (prefix, header = null) => {
    return axios
      .delete(baseUrl + prefix, {
        headers: header !== null ? header : defaultHeader(),
      })
      .then(res => Promise.resolve({data: res.data, status: res.status}))
      .catch(err => Promise.reject(err));
  },
  put: async (prefix, body, header = null) => {
    return axios
      .put(baseUrl + prefix, body, {
        headers: header !== null ? header : defaultHeader(),
      })
      .then(res => Promise.resolve({data: res.data, status: res.status}))
      .catch(err => Promise.reject(err));
  },
};
