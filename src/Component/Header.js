import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/Feather';

const Header = ({onBack}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onBack}
      style={styles.header}>
      <Icon name="chevron-left" size={25} color={'#000'} />
      <Text style={styles.back}>Kembali</Text>
    </TouchableOpacity>
  );
};

export default Header;

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 10,
    backgroundColor: '#FFF',
  },
  back: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000',
    marginLeft: 10,
  },
});
