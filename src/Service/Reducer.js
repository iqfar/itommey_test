import {TOKEN} from './Case';
import {combineReducers} from 'redux';

const initialState = {
  id: 0,
  token: '',
  name: '',
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOKEN:
      return {
        ...state,
        token: action.token,
      };
    default:
      return state;
  }
};

const reducer = combineReducers({
  user: userReducer,
});

export default reducer;
