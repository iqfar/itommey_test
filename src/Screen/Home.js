import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {request} from '../Api';
import moment from 'moment';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';

const Home = () => {
  const [data, setData] = useState([]);
  const navigation = useNavigation();

  const getData = async () => {
    const url = '/product';
    try {
      const res = await request.get(url);
      if (res) {
        setData(res.data.filter(item => item.isActive));
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => navigation.navigate('Detail', {id: item.id})}
        style={styles.item}>
        <View style={styles.picture}>
          <Image source={{uri: item.picture}} style={styles.image} />
        </View>
        <View style={styles.productName}>
          <Text style={styles.name}>{item.name}</Text>
        </View>
        <View style={styles.qty}>
          <Text style={styles.stock}>Stok:</Text>
          <Text style={styles.num}>{item.qty}</Text>
        </View>
        <View style={styles.qty}>
          <Text style={styles.stock}>Expired at:</Text>
          <Text style={styles.num}>
            {moment(item.expiredAt).format('DD MMMM YYYY')}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={(_, i) => i.toString()}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.list}
      />
      <TouchableOpacity activeOpacity={0.8} style={styles.add}>
        <Icon name="plus" size={25} color={'#FFF'} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    resizeMode: 'contain',
  },
  picture: {
    width: '40%',
    height: undefined,
    aspectRatio: 1 / 1,
    alignSelf: 'center',
  },
  item: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.5)',
    borderRadius: 10,
    padding: 20,
    marginHorizontal: 20,
    marginBottom: 10,
  },
  name: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#000',
    textAlign: 'center',
  },
  qty: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderColor: 'rgba(32, 33, 36, 0.1)',
    paddingBottom: 5,
    marginBottom: 5,
  },
  productName: {
    borderBottomWidth: 1,
    borderColor: 'rgba(32, 33, 36, 0.1)',
    paddingBottom: 5,
    marginBottom: 5,
  },
  num: {
    marginLeft: 20,
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000',
  },
  list: {
    paddingVertical: 20,
  },
  add: {
    backgroundColor: '#000',
    borderRadius: 100,
    position: 'absolute',
    right: 0,
    bottom: 0,
    padding: 15,
    margin: 15,
  },
});

export default Home;
