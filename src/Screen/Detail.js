import {ActivityIndicator, Image, StyleSheet, Text, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {request} from '../Api';
import moment from 'moment';
import Icon from 'react-native-vector-icons/Feather';
import Header from '../Component/Header';
import { useNavigation } from '@react-navigation/native';

const Detail = ({route}) => {
  const {id} = route.params;
  const navigation = useNavigation();
  const [data, setData] = useState();
  const getData = async id => {
    const url = '/product/' + id;
    try {
      const res = await request.get(url);
      if (res) {
        setData(res.data);
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };

  useEffect(() => {
    if (id) {
      getData(id);
    }
  }, [id]);

  if (data === undefined) {
    return <ActivityIndicator size={'large'} color={'#000'} />;
  }
  return (
    <View style={styles.container}>
      <Header onBack={() => navigation.goBack()} />
      <View style={styles.item}>
        <View style={styles.picture}>
          <Image source={{uri: data.picture}} style={styles.image} />
        </View>
        <View style={styles.productName}>
          <Text style={styles.name}>{data.name}</Text>
        </View>
        <View style={styles.qty}>
          <Text style={styles.stock}>Stok:</Text>
          <Text style={styles.num}>{data.qty}</Text>
        </View>
        <View style={styles.qty}>
          <Text style={styles.stock}>Expired at:</Text>
          <Text style={styles.num}>
            {moment(data.expiredAt).format('DD MMMM YYYY')}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default Detail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    resizeMode: 'contain',
  },
  picture: {
    width: '80%',
    height: undefined,
    aspectRatio: 1 / 1,
    alignSelf: 'center',
    marginBottom: 20,
  },
  item: {
    borderRadius: 10,
    padding: 20,
    marginHorizontal: 20,
    marginBottom: 10,
  },
  name: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#000',
    textAlign: 'center',
  },
  qty: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderColor: 'rgba(32, 33, 36, 0.1)',
    paddingBottom: 5,
    marginBottom: 5,
  },
  productName: {
    borderBottomWidth: 1,
    borderColor: 'rgba(32, 33, 36, 0.1)',
    paddingBottom: 5,
    marginBottom: 5,
  },
  num: {
    marginLeft: 20,
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000',
  },
  list: {
    paddingVertical: 20,
  },
});
