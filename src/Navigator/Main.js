import {createStackNavigator} from '@react-navigation/stack';
import Detail from '../Screen/Detail';
import Home from '../Screen/Home';

const StackNavigator = createStackNavigator();

const MainNavigator = () => {
  return (
    <StackNavigator.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <StackNavigator.Screen name="Home" component={Home} />
      <StackNavigator.Screen name='Detail' component={Detail} />
    </StackNavigator.Navigator>
  );
};

export default MainNavigator;
